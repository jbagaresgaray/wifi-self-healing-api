(function() {
    'use strict';

    angular.module('wifi')
        .controller('dashMonitoringCtrl', ['$scope', '$timeout', 'dataFactory', 'appSocket', function($scope, $timeout, dataFactory, appSocket) {
            $scope.mapsMarker = [];
            $scope.defaultMap = 'World';

            $scope.loadWorldMap = function() {
                $scope.defaultMap = 'World';

                angular.element(document).ready(function() {
                    $('#world-map-markers').remove();
                    $('<div id="world-map-markers" style="height: 500px;"></div>').appendTo($('.pad'));

                    $('#world-map-markers').vectorMap({
                        map: 'world_mill_en',
                        normalizeFunction: 'polynomial',
                        hoverOpacity: 0.7,
                        hoverColor: false,
                        backgroundColor: 'transparent',
                        regionStyle: {
                            initial: {
                                fill: 'rgba(141, 141, 141, 1)',
                                "fill-opacity": 1,
                                stroke: 'none',
                                "stroke-width": 0,
                                "stroke-opacity": 1
                            },
                            hover: {
                                "fill-opacity": 0.7,
                                cursor: 'pointer'
                            },
                            selected: {
                                fill: 'yellow'
                            },
                            selectedHover: {}
                        },
                        markerStyle: {
                            initial: {
                                fill: '#00a65a',
                                stroke: '#111'
                            }
                        }
                    });

                    $('#world-map-markers').vectorMap('get', 'mapObject').addMarkers($scope.mapsMarker);
                });
            };

            $scope.loadAusMap = function() {
                angular.element(document).ready(function() {
                    $('#world-map-markers').remove();
                    $('<div id="world-map-markers" style="height: 500px;"></div>').appendTo($('.pad'));

                    $scope.defaultMap = 'Australia';

                    $('#world-map-markers').vectorMap({
                        map: 'au_mill',
                        normalizeFunction: 'polynomial',
                        hoverOpacity: 0.7,
                        hoverColor: false,
                        focusOn: {
                            x: 0.6,
                            y: -0.2,
                            scale: 1.2
                        },
                        backgroundColor: 'transparent',
                        regionStyle: {
                            initial: {
                                fill: 'rgba(141, 141, 141, 1)',
                                "fill-opacity": 1,
                                stroke: 'none',
                                "stroke-width": 0,
                                "stroke-opacity": 1
                            },
                            hover: {
                                "fill-opacity": 0.7,
                                cursor: 'pointer'
                            },
                            selected: {
                                fill: 'yellow'
                            },
                            selectedHover: {}
                        },
                        markerStyle: {
                            initial: {
                                fill: '#00a65a',
                                stroke: '#111'
                            }
                        }
                    });

                    $('#world-map-markers').vectorMap('get', 'mapObject').addMarkers($scope.mapsMarker);
                });
            };

            $scope.loadPhilMap = function() {
                angular.element(document).ready(function() {
                    $('#world-map-markers').remove();
                    $('<div id="world-map-markers" style="height: 500px;"></div>').appendTo($('.pad'));

                    $scope.defaultMap = 'Philippines';

                    $('#world-map-markers').vectorMap({
                        map: 'ph_mill_en',
                        normalizeFunction: 'polynomial',
                        hoverOpacity: 0.7,
                        hoverColor: false,
                        focusOn: {
                            x: 0.6,
                            y: -0.2,
                            scale: 1.2
                        },
                        backgroundColor: 'transparent',
                        regionStyle: {
                            initial: {
                                fill: 'rgba(141, 141, 141, 1)',
                                "fill-opacity": 1,
                                stroke: 'none',
                                "stroke-width": 0,
                                "stroke-opacity": 1
                            },
                            hover: {
                                "fill-opacity": 0.7,
                                cursor: 'pointer'
                            },
                            selected: {
                                fill: 'yellow'
                            },
                            selectedHover: {}
                        },
                        markerStyle: {
                            initial: {
                                fill: '#00a65a',
                                stroke: '#111'
                            }
                        }
                    });

                    $('#world-map-markers').vectorMap('get', 'mapObject').addMarkers($scope.mapsMarker);
                });
            };

            $scope.loadWorldMap();

            // SOCKET
            appSocket.on('devices', function(device) {
                device = JSON.parse(device);

                $scope.mapsMarker.push({ latLng: [device.latitude, device.longitude], name: device.deviceId });
                $('#world-map-markers').vectorMap('get', 'mapObject').addMarkers($scope.mapsMarker);
            });
        }]);
})();
