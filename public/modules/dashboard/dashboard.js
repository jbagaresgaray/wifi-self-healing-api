(function() {
    'use strict';

    angular.module('wifi')
        .config(['$stateProvider',
            function($stateProvider) {

                $stateProvider
                    .state('main.dashboard', {
                        url: '/dashboard',
                        templateUrl: 'public/modules/dashboard/dashboard.html'
                    });
            }
        ])
        .controller('dashboardCtrl', ['$rootScope', '$scope', 'appSocket', function($rootScope, $scope, appSocket) {
            var devices = [];
            var appLogs = [];

            $scope.info = {};

            // DEVICES
            $scope.$watch(function() {
                return $rootScope.devices || $scope.$parent.devices;
            }, function(newValue) {
                devices = newValue;

                _.each(devices, function(row) {
                    row.online = false;
                });

                $scope.info.offline = _.filter(devices, function(row) {
                    return row.online == false;
                });

                $scope.info.online = _.filter(devices, function(row) {
                    return row.online == true;
                });

                $scope.info.devices = devices.length;
            });

            // LOGS
            $scope.$watch(function() {
                return $rootScope.appLogs || $scope.$parent.appLogs;
            }, function(newValue) {
                if (!_.isEmpty(newValue)) {
                    appLogs = newValue;
                    $scope.info.appLogs = newValue.totalRows;
                }
            });



            // SOCKET
            appSocket.on('devices', function(device) {
                device = JSON.parse(device);

                _.each(devices, function(row) {
                    if (row.deviceID == device.deviceId) {
                        row.online = true;
                    }
                });

                $scope.info.online = _.filter(devices, function(row) {
                    return row.online == true;
                });
            });
        }]);
})();
