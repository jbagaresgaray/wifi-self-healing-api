(function() {
    'use strict';

    angular.module('wifi')
        .controller('appLogsCtrl', ['$rootScope', '$scope', '$timeout', function($rootScope, $scope, $timeout) {
           
            var appLogs = [];
            $scope.appLogs = [];

            var appLogs = $rootScope.appLogs || $scope.$parent.appLogs;

            $timeout(function() {
                $scope.$apply(function() {
                    $scope.appLogs = appLogs.rows;
                });
            }, 100);

            $scope.$watch(function() {
                return $rootScope.appLogs || $scope.$parent.appLogs;
            }, function(newValue) {

                appLogs = newValue;
                $scope.appLogs = newValue.rows;
            });
        }]);
})();
