(function() {
    'use strict';

    angular.module('wifi')
        .controller('dashboardMonitoringCtrl', ['$rootScope', '$scope', 'appSocket', function($rootScope, $scope, appSocket) {
            var devices = [];
            var appLogs = [];

            $scope.info = {};

            // DEVICES
            $scope.$watch(function() {
                return $rootScope.devices || $scope.$parent.devices;
            }, function(newValue) {
                devices = newValue;

                _.each(devices, function(row) {
                    row.online = false;
                });

                $scope.info.offline = _.filter(devices, function(row) {
                    return row.online == false;
                });

                $scope.info.online = _.filter(devices, function(row) {
                    return row.online == true;
                });

                $scope.info.devices = devices.length;
            });

            // LOGS
            $scope.$watch(function() {
                return $rootScope.appLogs || $scope.$parent.appLogs;
            }, function(newValue) {
                appLogs = newValue;

                $scope.info.appLogs = appLogs.length;
            });


            // SOCKET
            appSocket.on('devices', function(device) {
                device = JSON.parse(device);

                _.each(devices, function(row) {
                    if (row.deviceID == device.deviceId) {
                        row.online = true;
                    }
                });

                $scope.info.online = _.filter(devices, function(row) {
                    return row.online == true;
                });

            });
        }]);
})();
