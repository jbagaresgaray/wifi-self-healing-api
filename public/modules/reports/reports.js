(function() {
    'use strict';

    angular.module('wifi')
        .config(['$stateProvider',
            function($stateProvider) {

                $stateProvider
                    .state('main.applicationlog', {
                        url: '/applicationlog',
                        templateUrl: 'public/modules/reports/applicationlog.html',
                        controller: 'LogsCtrl'
                    });
            }
        ])
        .controller('LogsCtrl', LogsCtrl);

    LogsCtrl.$inject = ['$scope', '$uibModal', '$timeout', 'dataFactory'];

    function LogsCtrl($scope, $uibModal, $timeout, dataFactory) {

        $scope.selectDeselectAll = false;
        $scope.selectedDatas = [];

        var dateTo = new Date();
        $scope.dtFrom = dateTo.setMonth(dateTo.getMonth() - 1);
        $scope.dtTo = new Date();

        $scope.page = 0;
        $scope.limit = 50;
        $scope.maxSize = 15;

        var startDate = moment($scope.dtFrom).format("YYYY-MM-DD");
        var endDate = moment($scope.dtTo).format("YYYY-MM-DD");

        function Initialize() {
            $scope.appLogs = [];
            dataFactory.getAllAppLogs($scope.page, $scope.limit).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var appLogs = data.response.result;

                    $timeout(function() {
                        $scope.$apply(function() {
                            $scope.appLogs = appLogs.rows;
                            $scope.totalItems = appLogs.totalRows;
                        });
                    }, 100);
                }
            });
        }

        Initialize();

        $scope.pageChanged = function() {
            Initialize();
        };

        $scope.openDate = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: './public/modules/datepicker.modal.html',
                controller: 'ModalInstanceCtrl',
                size: 'sm',
                resolve: {
                    analyticsDate: function() {
                        return {
                            dateFrom: $scope.dtFrom,
                            dateTo: $scope.dtTo
                        };
                    }
                }
            });

            modalInstance.result.then(function(result) {
                $scope.dtFrom = result.dateFrom;
                $scope.dtTo = result.dateTo;

                initialize();
            });
        };

        $scope.refreshReport = function() {
            Initialize();
        };

        $scope.changeSelectDeselectAll = function() {
            _.each($scope.appLogs, function(data) {
                data.selected = $scope.selectDeselectAll;
                $scope.deleteSomeData = $scope.selectDeselectAll;
                $scope.checkDataChanges(data);
            });
        };

        $scope.checkDataChanges = function(store) {
            if (store.selected) {
                if (!_.find($scope.selectedDatas, function(selectedData) {
                        return selectedData._id === store._id;
                    })) {
                    $scope.selectedDatas.push(store);
                }
            } else {
                $scope.selectedDatas = _.filter($scope.selectedDatas, function(selectedData) {
                    return selectedData._id !== store._id;
                });
            }
            $scope.deleteSomeData = ($scope.selectedDatas.length > 0) ? true : false;
        };

    }
})();
