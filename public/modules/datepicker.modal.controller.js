(function() {
    'use strict';
    angular.module('wifi')
        .controller('ModalInstanceCtrl', ModalInstanceCtrl);

    ModalInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'analyticsDate'];

    function ModalInstanceCtrl($scope, $uibModalInstance, analyticsDate) {

        $scope.dtFrom = new Date(analyticsDate.dateFrom);
        $scope.dtTo = new Date(analyticsDate.dateTo);

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(3000, 12, 31),
            minDate: new Date(1800, 1, 1),
            startingDay: 1
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };

        $scope.openFrom = function() {
            $scope.popup1.opened = true;
        };

        $scope.openTo = function() {
            $scope.popup2.opened = true;
        };

        $scope.save = function() {
            $uibModalInstance.close({
                dateFrom: $scope.dtFrom,
                dateTo: $scope.dtTo
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    };
})();
