(function() {
    'use strict';
    angular.module('wifi')
        .controller('DeviceInfoInstanceCtrl', DeviceInfoInstanceCtrl);

    DeviceInfoInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'device', 'appSocket', '$timeout', '$interval'];

    function DeviceInfoInstanceCtrl($scope, $uibModalInstance, device, appSocket, $timeout, $interval) {

        var startTime;
        var logs = [];
       
        $scope.device = device
        $scope.pingLogs = [];
        $scope.dateNow = Date.now();

        appSocket.on('devices', function(connect) {
            connect = JSON.parse(connect);
            
            if (device.deviceID == connect.deviceId) {
                device.online = true;
            }
            $timeout(function() {
                $scope.$apply(function() {
                    $scope.device = device;
                });
            }, 100);
        });

        appSocket.on('device:ping:received', function(device) {
            var latency = Date.now() - device.startTime;
            logs.push({
                latency: latency,
                response: 'OK',
                device: device
            });

            $scope.pingLogs = logs;
        });

        appSocket.on('device:ping:stop', function(device) {
            var latency = Date.now() - device.startTime;
            logs.push({
                latency: latency,
                response: 'STOP - Device: INACTIVE (' + moment(Date.now()).format('MMMM Do YYYY, h:mm:ss a') + ')',
                device: device
            });

            $scope.pingLogs = logs;
        });

        

        $scope.cancel = function() {
            appSocket.removeListener('device:ping:received');            
            $uibModalInstance.dismiss('cancel');
        };
    };
})();
