(function() {
    'use strict';

    angular.module('wifi')
        .config(['$stateProvider',
            function($stateProvider) {

                $stateProvider
                    .state('main.devices', {
                        url: '/devices',
                        templateUrl: 'public/modules/devices/devices.html',
                        controller: 'deviceCtrl'
                    });
            }
        ])
        .controller('deviceCtrl', ['$scope', '$rootScope', '$timeout', '$uibModal', 'appSocket', '$filter',
            function($scope, $rootScope, $timeout, $uibModal, appSocket, $filter) {
                var devices = [];
                $scope.applicationStatus = 'All';
                $scope.devices = $rootScope.devices || $scope.$parent.devices;
                $scope.devicesCopy = $rootScope.devices || $scope.$parent.devices;

                $scope.$watch(function() {
                    return $rootScope.devices || $scope.$parent.devices;
                }, function(newValue) {
                    devices = newValue;

                    _.each(devices, function(row) {
                        row.online = false;
                    });

                    $scope.devicesCopy = devices;
                    $scope.devices = devices;
                });

                $scope.openDeviceInfo = function(device) {
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: './public/modules/devices/device.modal.html',
                        controller: 'DeviceInfoInstanceCtrl',
                        size: 'md',
                        resolve: {
                            device: function() {
                                return device;
                            }
                        }
                    });

                    modalInstance.result.then(function(result) {
                        $scope.dtFrom = result.dateFrom;
                        $scope.dtTo = result.dateTo;

                        initialize();
                    });
                };

                $scope.updateFilter = function(status) {
                    $scope.applicationStatus = status;
                    var devices = [];
                    if (status == 'All') {
                        devices = $scope.devicesCopy;
                    } else if (status == 'Active') {
                        devices = $filter('filter')($scope.devicesCopy, { 'online': true });
                    } else if (status == 'Inactive') {
                        devices = $filter('filter')($scope.devicesCopy, { 'online': false });
                    }

                    $timeout(function() {
                        $scope.$apply(function() {
                            $scope.devices = devices;
                        });
                        $('.table').trigger('footable_redraw');
                    }, 100);

                };

                appSocket.on('devices', function(device) {
                    device = JSON.parse(device);
                    console.log('device: ',device.deviceId);
                    
                    _.each(devices, function(row) {
                        if (row.deviceID == device.deviceId) {
                            console.log('row: ',row);
                            row.online = device.online || true;
                            row.socketId = device.socketId;
                        }
                    });

                    $timeout(function() {
                        $scope.$apply(function() {
                            $scope.devices = devices;
                        });
                    }, 100);
                });

                appSocket.on('networkState:device', function(device) {
                    console.log('networkState:device...', device);
                });

            }
        ]);
})();
