(function() {
    'use strict';

    angular.module('wifi')
        .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = ['$rootScope', '$scope', '$state', 'toastr', 'dataFactory', '$timeout'];

    function MainCtrl($rootScope, $scope, $state, toastr, dataFactory, $timeout) {
        // ROOTSCOPES
        $rootScope.devices = [];
        $rootScope.appLogs = [];
        // SCOPES
        $scope.devices = [];
        $scope.appLogs = [];

        var page = 0;
        var limit = 20;

        $scope.isDoneLoading = false;

        async.waterfall([
            function(callback) {
                dataFactory.getAllDevices().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        $rootScope.devices = data.response.result;
                        $scope.devices = data.response.result;
                        callback(null, data.response.result);
                    }
                });
            },
            function(devices) {
                $timeout(function() {
                    $scope.isDoneLoading = true;
                }, 3000);
            }
        ]);

        dataFactory.getAllAppLogs(page, limit).then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                $rootScope.appLogs = data.response.result;
                $scope.appLogs = data.response.result;
            }
        });


    }
})();
