(function() {
    'use strict';

    angular.module('wifi', [
            'ngSanitize',
            'ngAnimate',
            'ui.router',
            'chart.js',
            'ui.bootstrap',
            'ui.footable',
            'restangular',
            'toastr',
            'angularMoment',
            'angularSpinner',
            'ngDialog',
            'ui.select',
            'mgcrea.ngStrap',
            'LocalStorageModule',
            'angular-jwt',
            'btford.socket-io'
        ])
        // .constant('API_URL', 'http://localhost:3000')
        .constant('API_URL', 'http://wifi-self-healing.herokuapp.com/') // PRODUCTION
        .constant('API_VERSION', '/api/1.0/')
        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 'RestangularProvider', 'API_URL', 'API_VERSION', '$provide', 'localStorageServiceProvider',
            function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, RestangularProvider, API_URL, API_VERSION, $provide, localStorageServiceProvider) {

                $locationProvider.html5Mode(false);
                $urlRouterProvider.otherwise('/main/dashboard');
                $stateProvider
                    .state('main', {
                        url: '/main',
                        abstract: true,
                        templateUrl: 'public/modules/main.html',
                        controller: 'MainCtrl'
                    });

                $httpProvider.interceptors.push('httpInterceptor');
                $httpProvider.interceptors.push('authInterceptor');

                $provide.value('baseURL', API_URL);
                RestangularProvider.setBaseUrl(API_URL + API_VERSION);


            }
        ])
        .run(['$state', '$rootScope', 'usSpinnerService', '$window', 'jwtHelper', 'localStorageService', '$location', 'appSocket',
            function($state, $rootScope, usSpinnerService, $window, jwtHelper, localStorageService, $location, appSocket) {
                $rootScope.$state = $state;
                $rootScope.previousState;
                $rootScope.currentState;

                $rootScope.$on('loading:progress', function() {
                    usSpinnerService.spin('spinner-1');
                });

                $rootScope.$on('loading:finish', function() {
                    usSpinnerService.stop('spinner-1');
                });

                $rootScope.goBack = function() {
                    $window.history.back();
                };

                $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {
                    $rootScope.previousState = fromState.name;
                    $rootScope.currentState = toState.name;
                });

                appSocket.on('connect', function() {
                    // console.log('Socket Connected...');
                });
            }
        ])
        .factory('httpInterceptor', ['$q', '$rootScope',
            function($q, $rootScope) {
                var loadingCount = 0;

                return {
                    request: function(config) {
                        if (++loadingCount === 1) $rootScope.$broadcast('loading:progress');
                        return config || $q.when(config);
                    },

                    response: function(response) {
                        if (--loadingCount === 0) $rootScope.$broadcast('loading:finish');
                        return response || $q.when(response);
                    },

                    responseError: function(response) {
                        if (--loadingCount === 0) $rootScope.$broadcast('loading:finish');
                        return $q.reject(response);
                    }
                };
            }
        ]);


})();
