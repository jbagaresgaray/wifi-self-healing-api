(function() {
    'use strict';

    angular.module('wifi')
        .directive('errSrc', function() {
            return {
                link: function(scope, element, attrs) {
                    scope.$watch(function() {
                        return attrs['ngSrc'];
                    }, function(value) {
                        if (!value) {
                            element.attr('src', attrs.errSrc);
                        }
                    });

                    element.bind('error', function() {
                        element.attr('src', attrs.errSrc);
                    });
                }
            };
        })
        .directive('sidebar', function() {
            return {
                restrict: 'C',
                compile: function(tElement, tAttrs, transclude) {
                    //Enable sidebar tree view controls
                    $.AdminLTE.tree(tElement);
                }
            };
        })
        .directive('header', function() {
            return {
                restrict: 'E',
                templateUrl: 'public/modules/templates/header.html',
                compile: function(tElement, tAttrs, transclude) {
                    $.AdminLTE.pushMenu($(tElement).find('.sidebar-toggle'));
                }
            };
        })
        .directive('knob', function() {
            return {
                restrict: 'C',
                compile: function(tElement, tAttrs, transclude) {
                    tElement.knob({
                        draw: function() {

                            // "tron" case
                            if (this.$.data('skin') == 'tron') {

                                var a = this.angle(this.cv) // Angle
                                    ,
                                    sa = this.startAngle // Previous start angle
                                    ,
                                    sat = this.startAngle // Start angle
                                    ,
                                    ea // Previous end angle
                                    , eat = sat + a // End angle
                                    ,
                                    r = true;

                                this.g.lineWidth = this.lineWidth;

                                this.o.cursor && (sat = eat - 0.3) && (eat = eat + 0.3);

                                if (this.o.displayPrevious) {
                                    ea = this.startAngle + this.angle(this.value);
                                    this.o.cursor && (sa = ea - 0.3) && (ea = ea + 0.3);
                                    this.g.beginPath();
                                    this.g.strokeStyle = this.previousColor;
                                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                                    this.g.stroke();
                                }

                                this.g.beginPath();
                                this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                                this.g.stroke();

                                this.g.lineWidth = 2;
                                this.g.beginPath();
                                this.g.strokeStyle = this.o.fgColor;
                                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                                this.g.stroke();

                                return false;
                            }
                        }
                    });
                    /* END JQUERY KNOB */
                }
            }
        })
        .directive('sparkline', function() {
            return {
                restrict: 'C',
                compile: function(tElement, tAttrs, transclude) {
                    tElement.each(function() {
                        var $this = $(this);
                        $this.sparkline('html', $this.data());
                    });
                }
            }
        })
        .directive('box', function() {
            return {
                restrict: 'C',
                compile: function(tElement, tAttr, transclude) {
                    var _this = this;
                    $(tElement).find(this.boxWidgetOptions.boxWidgetSelectors.collapse).click(function(e) {
                        e.preventDefault();
                        _this.collapse($(this));
                    });
                    $(tElement).find(this.boxWidgetOptions.boxWidgetSelectors.remove).click(function(e) {
                        e.preventDefault();
                        _this.remove($(this));
                    });
                },
                collapse: function(element) {
                    //Find the box parent
                    var box = element.parents(".box").first();
                    //Find the body and the footer
                    var bf = box.find(".box-body, .box-footer");
                    if (!box.hasClass("collapsed-box")) {
                        //Convert minus into plus
                        element.children(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
                        bf.slideUp(300, function() {
                            box.addClass("collapsed-box");
                        });
                    } else {
                        //Convert plus into minus
                        element.children(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
                        bf.slideDown(300, function() {
                            box.removeClass("collapsed-box");
                        });
                    }
                },
                remove: function(element) {
                    //Find the box parent
                    var box = element.parents(".box").first();
                    box.slideUp();
                },
                boxWidgetOptions: {
                    boxWidgetIcons: {
                        //The icon that triggers the collapse event
                        collapse: 'fa fa-minus',
                        //The icon that trigger the opening event
                        open: 'fa fa-plus',
                        //The icon that triggers the removing event
                        remove: 'fa fa-times'
                    },
                    boxWidgetSelectors: {
                        //Remove button selector
                        remove: '[data-widget="remove"]',
                        //Collapse button selector
                        collapse: '[data-widget="collapse"]'
                    }
                }

            }
        })
        .directive("repeatEnd", function() {
            return {
                restrict: "A",
                link: function(scope, element, attrs) {
                    if (scope.$last) {
                        scope.$eval(attrs.repeatEnd);
                    }
                }
            };
        })
        .directive('ngRepeatEndWatch', function() {
            return {
                restrict: 'A',
                scope: {},
                link: function(scope, element, attrs) {
                    if (attrs.ngRepeat) {
                        if (scope.$parent.$last) {
                            if (attrs.ngRepeatEndWatch !== '') {
                                if (typeof scope.$parent.$parent[attrs.ngRepeatEndWatch] === 'function') {
                                    // Executes defined function
                                    scope.$parent.$parent[attrs.ngRepeatEndWatch]();
                                } else {
                                    // For watcher, if you prefer
                                    scope.$parent.$parent[attrs.ngRepeatEndWatch] = true;
                                }
                            } else {
                                // If no value was provided than we will provide one on you controller scope, that you can watch
                                // WARNING: Multiple instances of this directive could yeild unwanted results.
                                scope.$parent.$parent.ngRepeatEnd = true;
                            }
                        }
                    } else {
                        throw 'ngRepeatEndWatch: `ngRepeat` Directive required to use this Directive';
                    }
                }
            };
        })
        .directive('infiniteScroll', ['$rootScope', '$window', '$timeout', function($rootScope, $window, $timeout) {
            return {
                link: function(scope, elem, attrs) {
                    var checkWhenEnabled, handler, scrollDistance, scrollEnabled;
                    $window = angular.element($window);
                    elem.css('overflow-y', 'auto');
                    elem.css('overflow-x', 'hidden');
                    elem.css('height', 'inherit');
                    scrollDistance = 0;
                    if (attrs.infiniteScrollDistance != null) {
                        scope.$watch(attrs.infiniteScrollDistance, function(value) {
                            return (scrollDistance = parseInt(value, 10));
                        });
                    }
                    scrollEnabled = true;
                    checkWhenEnabled = false;
                    if (attrs.infiniteScrollDisabled != null) {
                        scope.$watch(attrs.infiniteScrollDisabled, function(value) {
                            scrollEnabled = !value;
                            if (scrollEnabled && checkWhenEnabled) {
                                checkWhenEnabled = false;
                                return handler();
                            }
                        });
                    }
                    $rootScope.$on('refreshStart', function() {
                        elem.animate({
                            scrollTop: "0"
                        });
                    });
                    handler = function() {
                        var container, elementBottom, remaining, shouldScroll, containerBottom;
                        container = $(elem.children()[0]);
                        elementBottom = elem.offset().top + elem.height();
                        containerBottom = container.offset().top + container.height();
                        remaining = containerBottom - elementBottom;
                        shouldScroll = remaining <= elem.height() * scrollDistance;
                        if (shouldScroll && scrollEnabled) {
                            if ($rootScope.$$phase) {
                                return scope.$eval(attrs.infiniteScroll);
                            } else {
                                return scope.$apply(attrs.infiniteScroll);
                            }
                        } else if (shouldScroll) {
                            return (checkWhenEnabled = true);
                        }
                    };
                    elem.on('scroll', handler);
                    scope.$on('$destroy', function() {
                        return $window.off('scroll', handler);
                    });
                    /*return $timeout((function() {
                        if (attrs.infiniteScrollImmediateCheck) {
                            if (scope.$eval(attrs.infiniteScrollImmediateCheck)) {
                                return handler();
                            }
                        } else {
                            return handler();
                        }
                    }), 0);*/
                    return $timeout(function() {
                        if (attrs.infiniteScrollImmediateCheck) {
                            if (scope.$eval(attrs.infiniteScrollImmediateCheck)) {
                                return handler();
                            }

                        } else {
                            return handler();
                        }
                    }, 0);
                }
            }

        }])
        .directive('starRating', function() {
            return {
                restrict: 'EA',
                template: '<ul class="star-rating" ng-class="{readonly: readonly}">' +
                    '  <li ng-repeat="star in stars" class="star" ng-class="{filled: star.filled}" ng-click="toggle($index)">' +
                    '    <i class="fa fa-star"></i>' + // or &#9733
                    '  </li>' +
                    '</ul>',
                scope: {
                    ratingValue: '=ngModel',
                    max: '=?', // optional (default is 5)
                    onRatingSelect: '&?',
                    readonly: '=?'
                },
                link: function(scope, element, attributes) {
                    if (scope.max == undefined) {
                        scope.max = 5;
                    }

                    function updateStars() {
                        scope.stars = [];
                        for (var i = 0; i < scope.max; i++) {
                            scope.stars.push({
                                filled: i < scope.ratingValue
                            });
                        }
                    };
                    scope.toggle = function(index) {
                        if (scope.readonly == undefined || scope.readonly === false) {
                            scope.ratingValue = index + 1;
                            scope.onRatingSelect({
                                rating: index + 1
                            });
                        }
                    };
                    scope.$watch('ratingValue', function(oldValue, newValue) {
                        if (newValue) {
                            updateStars();
                        }
                    });
                }
            };
        });

})();
