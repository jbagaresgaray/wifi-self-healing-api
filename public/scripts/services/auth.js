(function() {
    'use strict';
    angular.module('wifi')
        .factory('authInterceptor', authInterceptor);

    authInterceptor.$inject = ['$rootScope', '$q', '$location', '$injector', 'jwtHelper', 'localStorageService'];

    function authInterceptor($rootScope, $q, $location, $injector, jwtHelper, localStorageService) {
        return {
            request: function(config) {
                config.headers = config.headers || {};
                if (localStorageService.get('token')) {
                    var token = localStorageService.get('token');
                    config.headers.Authorization = 'Bearer ' + token;

                    var tokenPayload = jwtHelper.decodeToken(token);
                    if (tokenPayload) {
                        config.headers.accesskey = tokenPayload.subscriptionKey;
                    }
                }

                return config;
            },
            response: function(response) {
                //JWT Token: If the token is a valid JWT token, new or refreshed, save it in the localStorage
                if (localStorageService.get('token')) {
                    var storagedToken = localStorageService.get('token');
                    if (jwtHelper.isTokenExpired(storagedToken)) {
                        localStorageService.remove('token');
                        localStorageService.remove('user');

                        $location.path('/login');
                    }
                }
                return response;
            },
            responseError: function(response) {
                if (response.status === 401) {
                    $rootScope.$broadcast('unauthorized');
                    $location.path('/login');
                }
                return response
            }
        };
    }

})();
