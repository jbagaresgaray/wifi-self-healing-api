(function() {
    'use strict';
    angular.module('wifi')
        .factory('Socket', Socket)
        .factory('appSocket', appSocket);

    Socket.$inject = ['$rootScope', 'API_URL'];

    function Socket($rootScope, API_URL) {
        var socket = io.connect(API_URL);
        return {
            on: function(eventName, callback) {
                socket.on(eventName, function() {
                    var args = arguments;
                    $rootScope.$apply(function() {
                        callback.apply(socket, args);
                    });
                });
            },
            emit: function(eventName, data, callback) {
                socket.emit(eventName, data, function() {
                    var args = arguments;
                    $rootScope.$apply(function() {
                        if (callback) {
                            callback.apply(socket, args);
                        }
                    });
                });
            }
        };
    }


    appSocket.$inject = ['socketFactory', 'API_URL'];

    function appSocket(socketFactory, API_URL) {
        var myIoSocket = io.connect(API_URL);
        var socket = socketFactory({
            ioSocket: myIoSocket
        });
        return socket;
    }
})();
