'use strict';

var async = require('async');
var mongoose = require('mongoose');
var appLog = mongoose.model('AppLogs');
var sync = mongoose.model('SyncLogs');

// Create
exports.createAppLogs = function(data, next) {
    appLog.create(data, next);
};

// Get All appLogs
exports.getAllLogs = function(params, next) {
    params.page = (params.page == undefined) ? 0 : params.page;

    params.page = parseInt(params.page);
    params.limit = parseInt(params.limit);

    async.waterfall([
        function(callback) {
            appLog.find()
                .skip(params.limit * params.page)
                .limit(params.limit)
                .exec(callback);
        },
        function(datas, callback) {
            appLog.count({}, function(err, count) {
                callback(null, {
                    rows: datas,
                    totalRows: count
                });
            })
        }
    ], next);
};

// CreateSync
exports.createSyncLogs = function(data, next) {
    sync.create(data, next);
};

// Get All SyncLogs
exports.getAllSyncLogs = function(params, next) {
    sync.find(next);
};
