'use strict';

var async = require('async');
var mongoose = require('mongoose');
var device = mongoose.model('Devices');;


exports.getDeviceByUUID = function(uuid, next) {
    device.findOne({ 'deviceID': uuid }, next);
};

exports.getAllDevices = function(params,next){
    device.find(next);
};

exports.saveDeviceInfo = function(data, next) {
    device.findOneAndUpdate({
        'deviceID': data.deviceID
    },data, function(err, resp) {
        if (!err) {
            if (resp == null) {
                console.log('saveDeviceInfo Create: ',resp);
                device.create(data, next);
            }
        }
    });

};

exports.removeDeviceByUUID = function(uuid, next) {
    device.findOneAndRemove({ 'deviceID': uuid }, next);
};
