'use strict';

var devicedao = require('../daos/devicedao');

function App() {
    this.devicedao = devicedao;
}

App.prototype.getDeviceByUUID = function(uuid, next) {
    devicedao.getDeviceByUUID(uuid, function(err, response) {
        if (err) {
            next(err, null);
        }

        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
};

App.prototype.getAllDevices = function(params,next) {
    devicedao.getAllDevices(params, function(err, response) {
        if (err) {
            next(err, null);
        }

        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
};

App.prototype.saveDeviceInfo = function(data, next) {
    devicedao.saveDeviceInfo(data, function(err, response) {
        if (err) {
            next(err, null);
        }

        next(null, {
            result: response,
            msg: 'Device Info successfully saved!!!',
            success: true
        });
    });
};




App.prototype.removeDeviceByUUID = function(uuid, next) {
    devicedao.removeDeviceByUUID(uuid, function(err, response) {
        if (err) {
            next(err, null);
        }

        next(null, {
            result: response,
            msg: 'Device successfully deleted',
            success: true
        });
    });
};

exports.App = App;
