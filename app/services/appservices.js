'use strict';

var appDaos = require('../daos/appdaos');

function App() {
    this.appDaos = appDaos;
}

App.prototype.createAppLogs = function(data, next) {
    appDaos.createAppLogs(data, function(err, response) {
        if (err) {
            next(err, null);
        }

        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
};

App.prototype.getAllLogs = function(params, next) {
    appDaos.getAllLogs(params, function(err, response) {
        if (err) {
            next(err, null);
        }

        next(null, {
            result: response,
            msg: 'List of App Logs',
            success: true
        });
    });
};




App.prototype.createSyncLogs = function(data, next) {
    appDaos.createSyncLogs(data, function(err, response) {
        if (err) {
            next(err, null);
        }

        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
};

App.prototype.getAllSyncLogs = function(params, next) {
    appDaos.getAllSyncLogs(params, function(err, response) {
        if (err) {
            next(err, null);
        }

        next(null, {
            result: response,
            msg: 'List of Sync Logs',
            success: true
        });
    });
};

exports.App = App;
