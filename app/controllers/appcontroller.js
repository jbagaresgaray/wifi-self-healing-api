'use strict';

var cb = require('./../utils/callback');
var appServices = require('../services/appservices').App;
var app = new appServices();



exports.getAllLogs = function(req, res) {
    app.getAllLogs(req.query,cb.setupResponseCallback(res));
};

exports.createAppLogs = function(req, res) {
	app.createAppLogs(req.body,cb.setupResponseCallback(res));
};


exports.getAllSyncLogs = function(req, res) {
    app.getAllSyncLogs(req.query,cb.setupResponseCallback(res));
};

exports.createSyncLogs = function(req, res) {
    app.createSyncLogs(req.body,cb.setupResponseCallback(res));
};

