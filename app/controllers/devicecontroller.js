'use strict';

var cb = require('./../utils/callback');
var deviceService = require('../services/deviceservice').App;
var app = new deviceService();



exports.getDeviceByUUID = function(req, res) {
    app.getDeviceByUUID(req.params.uuid,cb.setupResponseCallback(res));
};

exports.getAllDevices = function(req, res) {
    app.getAllDevices(req.query,cb.setupResponseCallback(res));
};

exports.saveDeviceInfo = function(req, res) {
	app.saveDeviceInfo(req.body,cb.setupResponseCallback(res));
};

exports.removeDeviceByUUID = function(req, res) {
    app.removeDeviceByUUID(req.params.uuid,cb.setupResponseCallback(res));
};


