'use strict';

var fs = require('fs-extra');
var path = require('path');

// ======================== VALIDATION ============================ //
var validatorApp = require('../validations/about');

// ========================= ROUTING ============================== //

var deviceCtrl = require('../controllers/devicecontroller');
var appCtrl = require('../controllers/appcontroller');


module.exports = function(app, config) {

    app.route('/').get(function(req, res) {
        res.render('index');
    });

    app.route('/app').get(function(req, res) {
        res.render('app');
    });


    app.route('/docs-v1')
        .get(function onRequest(req, res) {
            var file = 'public/swagger/swagger.json';
            var dir = path.resolve(__dirname + '../../../' + file);
            console.log('file: ', file);
            fs.readFile(file, 'utf8', function(err, data) {
                if (err) {
                    console.log('Error: ' + err);
                    return;
                }
                data = JSON.parse(data);
                res.send(data);
            });
        });


    app.route('/docs')
        .get(function onRequest(req, res) {
            var file = 'public/swagger/dist/index.html';
            var dir = path.resolve(__dirname + '../../../' + file);
            console.log('file: ', file);
            fs.readFile(file, 'utf8', function(err, data) {
                if (err) {
                    console.log('Error: ' + err);
                    return;
                }
                res.send(data);
            });
        });

    app.route(config.api_version + '/app').get(validatorApp.validateAbout, appCtrl.getAllLogs);
    app.route(config.api_version + '/app').post(appCtrl.createAppLogs);

    app.route(config.api_version + '/appsync').get(appCtrl.getAllSyncLogs);
    app.route(config.api_version + '/appsync').post(appCtrl.createSyncLogs);

    app.route(config.api_version + '/devices').get(deviceCtrl.getAllDevices);

};
