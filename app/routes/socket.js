'use strict';

var socketio = require('socket.io');

var deviceService = require('../services/deviceservice').App;
var device = new deviceService();

module.exports = function(io) {
    var usernames = {};
    var socketConnection = {};

    var devicesArr = [];

    io.on('connection', function(socket) {
        console.log('--socket.io ' + socket.id + ' connected');

        socket.on('socket', function(data) { // CRUD socketIO
            var content = JSON.parse(data);

            content.socketId = socket.id;

            device.saveDeviceInfo({
                socketId: content.socketId,
                deviceID: content.deviceId,
                deviceManufacturer: content.manufacturer,
                deviceModel: content.model,
                osName: content.platform,
                osVersion: content.version,
                geolocation_lat: content.latitude,
                geolocation_lng: content.longitude,
                IPAddress: content.ipAddress,
                MacAddress: content.macAddress
            }, function(err, content) {
                console.log('Device saved: ');
            });
            content.online = true;
            
            console.log('connected device: ',content);
            socket.broadcast.emit('devices', content);
        });

        socket.on('networkState', function(connect) {
            socket.broadcast.to(connect.deviceId).emit('networkState:device', connect);
        });

        socket.on('app:action', function(connect) {
            socket.broadcast.to(connect.deviceId).emit('app:choose:me', connect);
        });

        socket.on('device:ping:send', function(device) {
            console.log('device ping: ', device.deviceID, ' socket: ', device.socketId);
            socket.broadcast.emit('devices', device);
            socket.broadcast.emit('device:ping:received', device);
        });

        socket.on('device:ping:stop', function(device) {
            console.log('device ping stop: ', device.deviceID, ' socket: ', device.socketId);
            socket.broadcast.emit('device:ping:stop', device);
        });


        socket.on('send:message', function(messageInfo) {
            var emitData = {
                from: messageInfo.from,
                to: messageInfo.to,
                name: messageInfo.name,
                deviceId: messageInfo.deviceId,
                content: messageInfo.content,
                time: messageInfo.time,
                status: 1
            };
            io.sockets.socket(socketConnection[emitData.to].id).emit('receipt:message', emitData);
        });

        socket.on('forceDisconnect',function(device){
            var socketId = socket.id;

            device.online = false;

            console.log('device disconnect to server', device);
            console.log('socketId disconnect to server', socketId);

            socket.broadcast.emit('devices', device);
        });

        socket.on('disconnect', function(device) {
            var socketId = socket.id;
            console.log('device disconnect to server', device);
            console.log('socketId disconnect to server', socketId);
        })
    });
};
