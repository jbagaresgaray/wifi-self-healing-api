'use strict';

module.exports = function(mongoose) {

    var LogsSchema = new mongoose.Schema({
        deviceID: String,
        deviceName: String,
        deviceModel: String,
        osName: String,
        osVersion: String,
        geolocation_lat: String,
        geolocation_lng: String,
        errorNumber: String,
        errorDescription: String,
        errorCompleteDetails: String,
        IPAddress: String,
        AppStatus: String,
        Interval: Number,
        dateConnected: Date,
        dateDisconnected: Date,
        dateCreated: {
            type: Date,
            default: Date.now
        }
    });


    var SyncLogSchema = new mongoose.Schema({
        SyncDate: {
            type: Date,
            default: Date.now
        },
        deviceID: String,
        deviceName: String,
        deviceModel: String,
        osName: String,
        osVersion: String,
        geolocation_lat: String,
        geolocation_lng: String,
        syncDescription: String,
        IPAddress: String
    });

    var DeviceSchema = new mongoose.Schema({
        socketId: String,
        deviceID: String,
        deviceManufacturer: String,
        deviceModel: String,
        osName: String,
        osVersion: String,
        geolocation_lat: String,
        geolocation_lng: String,
        IPAddress: String,
        MacAddress: String,
        dateConnected: {
            type: Date,
            default: Date.now
        }
    });


    mongoose.model('AppLogs', LogsSchema);
    mongoose.model('SyncLogs', SyncLogSchema);
    mongoose.model('Devices', DeviceSchema);
};
