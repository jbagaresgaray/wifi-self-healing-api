'use strict';

module.exports = function(database, server, config, log) {
    database.connect(function onConnect(err, isConnected) {
        if (err) {
            log.error('Error Connecting Mongodb database');
        } else {
            server.listen(process.env.PORT || config.port, function connection(err) {
                if (err instanceof Error) {
                    log.error('Unable to start SERVER at PORT: ', config.port);
                } else {
                    log.info('ENVIRONMENT: ' + config.env + ' Server started at PORT: ' + config.port + ' Using API VERSION: ' + config.api_version);
                }
            });
        }
    });
};
