'use strict';

exports.validateAbout = function(req, res, next) {
    req.checkQuery('limit', 'Please provide Data Limit').notEmpty();
    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};
