'use strict';

module.exports = {
    env : 'development',
    dbUrl: process.env.DB_URL || 'mongodb://localhost:27017/wifihealing',
    db_user: process.env.DB_USER || 'wifihealing',
    db_password: process.env.DB_USER || '12345',
    port: process.env.PORT || process.env.APP_PORT || 3000, // PLEASE DONT REMOVE 'process.env.PORT'
    ip: process.env.IP,
    socket_port: process.env.SOCKET_PORT || 3333,
    app_name: process.env.APP_NAME || "wifihealing",
    api_host_url: process.env.API_HOST_URL || 'http://localhost:3000',
    frontend_host_url: process.env.FRONTEND_HOST_URL || 'http://192.168.1.45:9000',
    api_version: process.env.API_VERSION || '/api/1.0',
    mailgun:{
        public_key: 'pubkey-7112567849ee43064f008c15c084a532',
        api_key: 'key-2564478fd7b4b93f24b7b9c5072661e9',
        domain: 'sandbox300149f3d3f74fc5b655c0b98d6ff20e.mailgun.org'
    }
};
