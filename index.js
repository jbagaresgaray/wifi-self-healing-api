'use strict';

process.env.TZ = 'UTC';

require('./config/env')();
var env = process.env.NODE_ENV || 'development';
process.env.NODE_ENV = env;

var application = require('./config/application'),
    express = require('express'),
    bunyan = require('bunyan'),
    mongoose = require('mongoose'),
    ejwt = require('express-jwt'),
    jwt = require('jsonwebtoken'),
    passport = require('passport'),
    middleware = require('./app/utils/middleware'),
    config = require('./config/environment/' + env),
    Database = require('./app/utils/database').Database,
    db = new Database(mongoose, config),
    log = bunyan.createLogger({
        name: config.app_name
    }),
    http = require('http'),
    app = express(),
    server = http.createServer(app),
    io = require('socket.io').listen(server, {
        'transports': ['xhr-polling', 'polling', 'websocket', 'flashsocket'],
        'origins': '*:*'
    });

// io.set('log level', 1);

var router = express.Router({
    strict: true,
    caseSensitive: true
});


process.env.NODE_ENV = env;

require(application.utils + 'helper')(db, server, config, log);
require(application.utils + 'loadschema')(mongoose);
require(application.config + 'express')(app, passport, config, ejwt);

// ===================== ROUTES ===================== //
require(application.routes + '/')(app, config);
require(application.routes + 'socket')(io);

module.exports = app;
