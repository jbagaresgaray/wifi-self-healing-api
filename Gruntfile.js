module.exports = function (grunt) {
    grunt.initConfig({
	  mongobackup: {
	    options: {
	      host : 'localhost',
	      port: '27017',
	      db : 'wifihealing', 
	      dump:{
	        out : './dump',
	      },    
	      restore:{
	        path : './dump/wifihealing',          
	        drop : true
	      }
	    }  
	  }
	});

	grunt.loadNpmTasks('grunt-mongo-backup');
}